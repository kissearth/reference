package com.reference.kafka.stream;

import org.apache.kafka.clients.admin.NewTopic;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

// @Configuration
public class KafkaConfig {

    @Bean
    public NewTopic inputTopic() {
        return new NewTopic("streams-plaintext-input", 1, (short) 1);
    }

    @Bean
    public NewTopic outputTopic() {
        return new NewTopic("streams-wordcount-output", 1, (short) 1);
    }

}

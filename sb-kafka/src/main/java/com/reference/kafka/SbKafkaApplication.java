package com.reference.kafka;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.core.env.Environment;

import java.net.InetAddress;
import java.net.UnknownHostException;

@SpringBootApplication
public class SbKafkaApplication {

    public static void main(String[] args) {
        BeanFactory beanFactory = SpringApplication.run(SbKafkaApplication.class, args);
        System.out.println("=======================================================");
        transferBeanFactory(beanFactory);
        System.out.println("=======================================================");
    }

    private static void transferBeanFactory(BeanFactory beanFactory) {
        String url = buildUrl(beanFactory.getBean(Environment.class));
        System.out.println("服务启动成功...，接口地址：" + buildUrl(beanFactory.getBean(Environment.class)));
        System.out.println("服务启动成功...，接口文档地址：" + url + "/doc.html");
    }

    private static String buildUrl(Environment env) {
        StringBuilder url = new StringBuilder("http://");
        try {
            url.append(InetAddress.getLocalHost().getHostAddress());
        } catch (UnknownHostException e) {
            url.append("127.0.0.1");
            e.printStackTrace();
        }
        String port = env.getProperty("server.port");
        url.append(":").append(port == null ? "8080" : port);
        String contextPath = env.getProperty("server.servlet.context-path");
        url.append(contextPath == null ? "/" : contextPath);
        return url.toString();
    }

}

package com.reference.kafka.pkg1;

import org.apache.kafka.clients.producer.Partitioner;
import org.apache.kafka.common.Cluster;

import java.util.Map;

/**
 * 自定义分区策略
 * <p>
 * 配置自定义分区器，配置的值就是分区器类的全路径名:
 * spring.kafka.producer.properties.partitioner.class=com.reference.kafka.pkg1.CustomizePartitioner
 */
public class CustomizePartitioner implements Partitioner {

    @Override
    public int partition(String topic, Object key, byte[] keyBytes, Object value, byte[] valueBytes, Cluster cluster) {
        // 自定义分区规则(这里假设全部发到0号分区)
        // ......
        return 0;
    }

    @Override
    public void close() {

    }

    @Override
    public void configure(Map<String, ?> configs) {

    }

}
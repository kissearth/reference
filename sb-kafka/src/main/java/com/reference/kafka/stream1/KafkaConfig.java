package com.reference.kafka.stream1;

import org.apache.kafka.clients.admin.NewTopic;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

// @Configuration
public class KafkaConfig {

    @Bean
    public NewTopic inputTopic() {
        return new NewTopic("input_topic", 1, (short) 1);
    }

    @Bean
    public NewTopic outputTopic() {
        return new NewTopic("output_topic", 1, (short) 1);
    }

}

package com.reference.kafka.pkg;

import com.alibaba.fastjson.JSON;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ProducerController {

    @Autowired
    KafkaTemplate<String, String> kafka;

    @GetMapping("/register")
    public String register() {
        User user = new User();
        String message = JSON.toJSONString(user);
        System.out.println("接收到用户信息：" + message);
        kafka.send("test", message);
        //kafka.send(String topic, @Nullable V data) {
        return "OK";
    }

}

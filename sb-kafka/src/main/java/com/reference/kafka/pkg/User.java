package com.reference.kafka.pkg;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Random;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class User implements Serializable {

    private String id = System.currentTimeMillis() + "";

    private String name = "����" + (int) new Random().nextDouble() * 100;

    private Integer age = (int) new Random().nextDouble() * 100;

}